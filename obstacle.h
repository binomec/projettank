#ifndef OBSTACLE_H
#define OBSTACLE_H
#include <QGraphicsItem>
#include <QPixmap>

class Obstacle : public QGraphicsPixmapItem
{
private:
    int resistance;
public:
    Obstacle();
    void setResistance(int val);
    int getResistance();

};

#endif // OBSTACLE_H
