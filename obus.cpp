#include "obus.h"
#include <QTimer>
#include <qmath.h> // qSin, qCos, qTan
#include "Game.h"
#include "impact.h"
#include <typeinfo>
#include "crevasse.h"
#include <iostream>
extern Game * game;

Obus::Obus(int r, int d, int q):QObject(),QGraphicsEllipseItem()
{
    setBrush(* new QBrush(QColor(0, 0, 0)));
    setRect(0,0,10,10);

    rayon = r;
    degats = d;
    quantite = q;

    move_timer = new QTimer(this);
    connect(move_timer,SIGNAL(timeout()),this,SLOT(se_deplacer()));


}

int Obus::getrayon()
{
    return rayon;
}

int Obus::getDegats()
{
    return degats;
}

int Obus::getQuantite()
{
    return quantite;
}

void Obus::setRayaon(int val)
{
    rayon = val;
}

void Obus::setDegats(int val)
{
    degats = val;
}

void Obus::setQuantite(int val)
{
    quantite = val;
}

void Obus::se_deplacer(){



            QList<QGraphicsItem *> colliding_items = collidingItems();

            for (int i = 0, n = colliding_items.size(); i < n; ++i){
                if (typeid(*(colliding_items[i])) == typeid(Impact)){

                    game->scene->removeItem(colliding_items[i]);

                    delete colliding_items[i];

                    QList<QGraphicsItem *> colliding_obstacles = collidingItems();

                        if(colliding_obstacles.size() == 0)
                        {
                            Crevasse * crevasse = new Crevasse(rayon);
                            crevasse->setPos(x(),y());
                            crevasse->centraliser();
                            crevasse->setZValue(-1);
                            game->scene->addItem(crevasse);

                            goto etiquette;
                        }


                    for (int j = 0, m = colliding_obstacles.size(); j < m; ++j){
                        if ( Obstacle * obstacle = dynamic_cast<Obstacle *>(colliding_obstacles[i])){


                            //obstacle->resistance = obstacle->resistance - degats;
                            obstacle->setResistance(obstacle->getResistance() - degats);

                            if(obstacle->getResistance()<= 0 && obstacle->getResistance() >(-100)){
                                scene()->removeItem(obstacle);
                                delete obstacle;

                                Crevasse * crevasse = new Crevasse(rayon);
                                crevasse->setPos(x(),y());
                                crevasse->centraliser();
                                crevasse->setZValue(-1);
                                game->scene->addItem(crevasse);

                                goto etiquette;
                            }

                        }

                        if ( Crevasse * ancienne_crevasse = dynamic_cast<Crevasse *>(colliding_obstacles[i])){

                            if(ancienne_crevasse->getRayon() <  rayon){

                                Crevasse * crevasse = new Crevasse(rayon);
                                crevasse->setPos(x(),y());
                                crevasse->centraliser();
                                crevasse->setZValue(-1);
                                game->scene->addItem(crevasse);
                            }

                        }

                        Tank * victime;
                        Canon * canon_victime;
                        if ((victime = dynamic_cast<Tank *>(colliding_obstacles[i]))||(canon_victime = dynamic_cast<Canon *>(colliding_obstacles[i]))){

                            if(!victime){
                                QList<QGraphicsItem *> cherche_tank = canon_victime->collidingItems();
                                for (int k = 0, o = cherche_tank.size(); k < o; ++k){
                                    if ( victime = dynamic_cast<Tank *>(cherche_tank[k])){ goto suite;}
                                }

                            }
                            suite:
                            victime->setVie(victime->getVie() - degats);

                            if(victime->getVie()<= 0){
                                game->scene->removeItem(victime);
                                game->scene->removeItem(victime->getCanon());
                                delete victime->getCanon();
                                delete victime;

                                Crevasse * crevasse = new Crevasse(rayon);
                                crevasse->setPos(x(),y());
                                crevasse->centraliser();
                                crevasse->setZValue(-1);
                                game->scene->addItem(crevasse);

                                game->gameover = true;
                                goto etiquette;
                            }

                        }
                    }


                    etiquette:
                    game->scene->removeItem(this);
                    delete this;

                    //passer le tour
                    game->passer_tour();
                    return;
                }
            }



            int STEP_SIZE = 10;
            double theta = rotation();

            double dy = STEP_SIZE * qSin(qDegreesToRadians(theta));
            double dx = STEP_SIZE * qCos(qDegreesToRadians(theta));

            setPos(x()+dx, y()+dy);



}

void Obus::lancer(){
    move_timer->start(50);
}





