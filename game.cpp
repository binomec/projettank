#include "game.h"
#include <QGraphicsRectItem>
#include <QBrush>
#include <QColor>
#include <QTime>
#include "arbre.h"
#include "crevasse.h"
#include "eau.h"
#include "roche.h"
#include "obstacle.h"
#include <QList>
#include <typeinfo>
#include <QMessageBox>


Game::Game(): QGraphicsView()
{
    //initialisation du seed pour random
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    //create a scene
     scene = new QGraphicsScene(this);
     scene->setSceneRect(0,0,800,600);
     //couleur de l'arriere plan (à transferer à terrain)
     //scene->setBackgroundBrush(* new QBrush(QColor(153, 102, 51)));
     setBackgroundBrush(QBrush(QImage(":/images/images/dirt.png")));
     //set the scene
     setScene(scene);
     setFixedSize(1200,600);
     setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
     setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);







   generer_obstacles(10,9,10);



   //tank
  tank = new Tank(true);
  positionner_tank(tank,true);

  tank2 = new Tank(false);
  positionner_tank(tank2,false);
  tank2->getCanon()->setRotation(180);
  tank2->getCanon()->setDirection(180);

   tank->setFlag(QGraphicsItem::ItemIsFocusable);
   tank2->setFlag(QGraphicsItem::ItemIsFocusable);

   tank->setFocus();
   tank->setPeuttirer(true);
   tour = true;
   gameover = false;

    info = new Infotank(tour,gameover,tank,tank2);
    scene->addItem(info->tank1);
    scene->addItem(info->deplacement1);
    scene->addItem(info->deplacement2);
    scene->addItem(info->obus1);
    scene->addItem(info->obus2);
    scene->addItem(info->tank1);
    scene->addItem(info->tank2);
    scene->addItem(info->tour1);
    scene->addItem(info->tour2);
    scene->addItem(info->vie1);
    scene->addItem(info->vie2);

    scene->addItem(info->canon1);
    scene->addItem(info->angle1);
    scene->addItem(info->direction1);
    scene->addItem(info->canon2);
    scene->addItem(info->angle2);
    scene->addItem(info->direction2);

}

bool Game::obstaclebon(Obstacle *obs)
{

    QList<QGraphicsItem *> obstacles = obs->collidingItems();
    for (int i = 0, n = obstacles.size(); i < n; ++i){
            if ((dynamic_cast<Obstacle *>(obstacles[i]))||(dynamic_cast<Tank *>(obstacles[i]))){
                // increase the score
                return false;


            }
        }
    return true;

}
bool Game::tankbon(Tank * tank){
    QList<QGraphicsItem *> colliding_items = tank->collidingItems();
    for (int i = 0, n = colliding_items.size(); i < n; ++i){
            if ((dynamic_cast<Obstacle *>(colliding_items[i]))||(dynamic_cast<Tank *>(colliding_items[i]))){

                return false;


            }
        }
    return true;
}

void Game::passer_tour()
{
    if(! gameover){
        actualiser();
        if(tour)
        {tank2->setFocus();
            tank2->setPeuttirer( true);
        tour = false;}
        else
        { tank->setFocus();
            tank->setPeuttirer(true);
        tour = true;}
    }
    else{
        QString  msgBox;
        if(tank) msgBox ="joueur1 a gagné";
        else msgBox ="joueur2 a gagné";



        QMessageBox::StandardButton reply;
          reply = QMessageBox::question(this, "terminé",msgBox ,
                                        QMessageBox::Yes|QMessageBox::No);
          if (reply == QMessageBox::Yes) {

            rejouer = true;
            this->close();

          } else {
              rejouer = false;
              this->close();
          }
    }
}

void Game::actualiser()
{
    scene->removeItem(info->tank1);
    scene->removeItem(info->deplacement1);
    scene->removeItem(info->deplacement2);
    scene->removeItem(info->obus1);
    scene->removeItem(info->obus2);
    scene->removeItem(info->tank1);
    scene->removeItem(info->tank2);
    scene->removeItem(info->tour1);
    scene->removeItem(info->tour2);
    scene->removeItem(info->vie1);
    scene->removeItem(info->vie2);

    scene->removeItem(info->canon1);
    scene->removeItem(info->angle1);
    scene->removeItem(info->direction1);
    scene->removeItem(info->canon2);
    scene->removeItem(info->angle2);
    scene->removeItem(info->direction2);
    delete info;
    info = new Infotank(tour,gameover,tank,tank2);
    scene->addItem(info->tank1);
    scene->addItem(info->deplacement1);
    scene->addItem(info->deplacement2);
    scene->addItem(info->obus1);
    scene->addItem(info->obus2);
    scene->addItem(info->tank1);
    scene->addItem(info->tank2);
    scene->addItem(info->tour1);
    scene->addItem(info->tour2);
    scene->addItem(info->vie1);
    scene->addItem(info->vie2);

    scene->addItem(info->canon1);
    scene->addItem(info->angle1);
    scene->addItem(info->direction1);
    scene->addItem(info->canon2);
    scene->addItem(info->angle2);
    scene->addItem(info->direction2);
}

void Game::generer_obstacles(int arbre, int roche, int eau)
{
    for (int i = 0; i < arbre; ++i){
         Arbre* arbre = new Arbre();
         scene->addItem(arbre);
         while(!obstaclebon(arbre)){
             scene->removeItem(arbre);
             delete arbre;
             arbre = new Arbre();
             scene->addItem(arbre);
         }
    }
    for (int i = 0; i < roche; ++i){
         Roche* roche = new Roche();
         scene->addItem(roche);
         while(!obstaclebon(roche)){
             scene->removeItem(roche);
             delete roche;
             roche = new Roche();
             scene->addItem(roche);
         }
    }
    for (int i = 0; i < eau; ++i){
         Eau* eau = new Eau();
         scene->addItem(eau);
         while(!obstaclebon(eau)){
             scene->removeItem(eau);
             delete eau;
             eau = new Eau();
             scene->addItem(eau);
         }
    }
}

void Game::positionner_tank(Tank *tank,bool cote)
{
    if(cote)
    {
        tank->positionner(qrand() % (351), qrand() % (551));
        scene->addItem(tank);
        scene->addItem(tank->getCanon());
        while(!tankbon(tank)){
            tank->positionner(qrand() % (351), qrand() % (551));
        }
    }
    else
    {
        tank->positionner(400 + (qrand() % (351)), qrand() % (551));
        scene->addItem(tank);
        scene->addItem(tank->getCanon());
        while(!tankbon(tank)){
            tank->positionner(400+ (qrand() % (351)), qrand() % (551));
        }
    }

}


