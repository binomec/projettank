#include "canon.h"
#include <QBrush>
#include <QColor>

Canon::Canon()
{
    setBrush(* new QBrush(QColor(128, 128, 128)));
    setRect(0,0,10,50);
}

void Canon::setDirection(int val)
{
    direction = val;
}

void Canon::setAngle(int val)
{
    angle = val;
}

void Canon::setL(float val)
{
    l = val;
}

int Canon::getDirection()
{
    return direction;
}

int Canon::getAngle()
{
    return angle;
}

float Canon::getL()
{
    return l;
}
