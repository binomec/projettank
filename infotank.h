#ifndef INFOTANK_H
#define INFOTANK_H

#include "tank.h"
#include <QBrush>
#include <QColor>
#include <QLabel>
#include <QGraphicsView>

class Infotank
{
public:
    Infotank(bool tour,bool gameover, Tank * char1,Tank * char2);
    QGraphicsTextItem * tank1;

    QGraphicsTextItem * tank2;

    QGraphicsTextItem * vie1;
    QGraphicsTextItem * deplacement1;
    QGraphicsTextItem * obus1;
    QGraphicsTextItem * tour1;

    QGraphicsTextItem * vie2;
    QGraphicsTextItem * deplacement2;
    QGraphicsTextItem * obus2;
    QGraphicsTextItem * tour2;

    QGraphicsTextItem * canon1;
    QGraphicsTextItem * angle1;
    QGraphicsTextItem * direction1;

    QGraphicsTextItem * canon2;
    QGraphicsTextItem * angle2;
    QGraphicsTextItem * direction2;


};

#endif // INFOTANK_H
