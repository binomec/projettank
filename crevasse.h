#ifndef CREVASSE_H
#define CREVASSE_H
#include <QGraphicsEllipseItem>

class Crevasse : public QGraphicsEllipseItem
{
public:
    Crevasse(int rayon);
    void setRayon(int val);
    int getRayon();
    void centraliser();
private:
    int rayon;
};

#endif // CREVASSE_H
