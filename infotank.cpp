#include "infotank.h"
#include "tank.h"
#include <QString>
Infotank::Infotank(bool tour,bool gameover, Tank * char1,Tank * char2)
{
    if(! gameover)
    {
        QString espace = " ";
        tank1 = new QGraphicsTextItem();
        tank1->setPlainText("Tank 1:");
        tank1->setPos(800,10);
        vie1 = new QGraphicsTextItem();
        vie1->setPlainText("vie: "+ QString::number(char1->getVie()));
        vie1->setPos(810,60);
        deplacement1 = new QGraphicsTextItem();
        deplacement1->setPlainText("cap.Dep : "+ QString::number(char1->getCapacite()));
        deplacement1->setPos(810,110);
        int num2 = char1->obus[1][2];
        int num3 = char1->obus[2][2];
        obus1 = new QGraphicsTextItem();
        obus1->setPlainText("obus1: infini"+espace+"obus2:"+espace+ QString::number(num2)+espace+"obus3:"+espace+ QString::number(num3));
        obus1->setPos(810,160);
        QString chaine = tour ? "oui" : "non";
        tour1 = new QGraphicsTextItem();
        tour1->setPlainText("tour: "+chaine);
        tour1->setPos(810,210);
        tank2 = new QGraphicsTextItem();
        tank2->setPlainText("Tank 2:");
        tank2->setPos(800,260);
        vie2 = new QGraphicsTextItem();
        vie2->setPlainText("vie: "+ QString::number(char2->getVie()));
        vie2->setPos(810,310);
        deplacement2 = new QGraphicsTextItem();
        deplacement2->setPlainText("cap.Dep : "+ QString::number(char2->getCapacite()));
        deplacement2->setPos(810,360);
        num2 = char2->obus[1][2];
        num3 = char2->obus[2][2];
        obus2 = new QGraphicsTextItem();
        obus2->setPlainText("obus1: infini"+espace+"obus2:"+espace+ QString::number(num2)+espace+"obus3:"+espace+ QString::number(num3));
        obus2->setPos(810,410);
        chaine = tour ? "non" : "oui";
        tour2 = new QGraphicsTextItem();
        tour2->setPlainText("tour: "+chaine);
        tour2->setPos(810,460);

        canon1= new QGraphicsTextItem();
        canon1->setPlainText("canon 1:");
        canon1->setPos(-200,10);
        angle1= new QGraphicsTextItem();
        angle1->setPlainText("Angle: "+ QString::number(char1->getCanon()->getAngle()));
        angle1->setPos(-150,60);
        direction1= new QGraphicsTextItem();
        direction1->setPlainText("Direction: "+QString::number(char1->getCanon()->getDirection()));
        direction1->setPos(-150,110);

        canon2= new QGraphicsTextItem();
        canon2->setPlainText("canon 2:");
        canon2->setPos(-200,160);
        angle2= new QGraphicsTextItem();
        angle2->setPlainText("Angle: "+ QString::number(char2->getCanon()->getAngle()));
        angle2->setPos(-150,210);
        direction2= new QGraphicsTextItem();
        direction2->setPlainText("Direction: "+QString::number(char2->getCanon()->getDirection()));
        direction2->setPos(-150,260);
    }
}
