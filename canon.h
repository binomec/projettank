#ifndef CANON_H
#define CANON_H
#include <QGraphicsRectItem>
#include "obus.h"


class Canon : public QGraphicsRectItem
{
public:
    Canon();
    void setDirection(int val);
    void setAngle(int val);
    void setL(float val);
    int getDirection();
    int getAngle();
    float getL();

private:
    int direction;
    int angle;
    float l;
};

#endif // CANON_H
