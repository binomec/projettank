#include "unitedep.h"

Unitedep::Unitedep(int x, int y)
{
  longueur = x;
  largeur = y;
}

void Unitedep::setLongueur(int val)
{
    longueur = val;
}

void Unitedep::setLargeur(int val)
{
    largeur = val;
}

int Unitedep::getLongueur()
{
    return longueur;
}

int Unitedep::getLageur()
{
    return largeur;
}
