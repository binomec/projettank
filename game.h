#ifndef GAME_H
#define GAME_H
#include "obstacle.h"
#include <QGraphicsView>
#include "tank.h"
#include "infotank.h"
class Game: public QGraphicsView
{


public:
    //member functions
    Game();
    QGraphicsScene * scene;
    bool obstaclebon(Obstacle * obs);
    void generer_obstacles(int arbre, int roche, int eau);
    void positionner_tank(Tank * tank, bool cote);
    bool tankbon(Tank * tank);
    void passer_tour();
    bool gameover;
    bool tour;
    Tank * tank ;
    Tank * tank2;
    Infotank * info;
    void actualiser();
    bool rejouer;

};


#endif // GAME_H
