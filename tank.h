#ifndef TANK_H
#define TANK_H

#include "canon.h"
#include <QGraphicsRectItem>
#include "crevasse.h"
#include "unitedep.h"
#include <QPixmap>

class Tank : public QGraphicsPixmapItem
{
public:
    Tank(bool couleur);
    QPointF tete_canon();
    void keyPressEvent(QKeyEvent * event);
    bool heurte_obstacle();
    Crevasse * heurte_crevasse();
    void traitement_mouvement(int a,int b);
    void deplacer(int x, int y);
    void positionner(int a, int b);
    void tirer(Obus * obus);
    int obus[3][3];
    Unitedep * getUnitedep();
    Canon * getCanon();
    bool getEncrevasse();
    bool getPeuttirer();
    int getCapacite();
    int getVie();

    void setEncrevasse(bool val);
    void setPeuttirer(bool val);
    void setCapacite(int val);
    void setVie(int val);

private:


    Unitedep * unitedep;
    Canon * canon;
    bool en_crevasse;
    bool peut_tirer;
    int capacite;//init Largeu du terrain / 10
    int vie;

};

#endif // TANK_H
