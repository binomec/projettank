#ifndef UNITEDEP_H
#define UNITEDEP_H


class Unitedep
{
public:
    Unitedep(int x, int y);
    void setLongueur(int val);
    void setLargeur(int val);
    int getLongueur();
    int getLageur();
private:
    int longueur;
    int largeur;
};

#endif // UNITEDEP_H
