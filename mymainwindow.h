#ifndef MYMAINWINDOW_H
#define MYMAINWINDOW_H
#include <QPushButton>
#include <QMessageBox>
#include <QMainWindow>
#include <QVBoxLayout>
#include "infotank.h"
class MyMainWindow: public QMainWindow
{
  Q_OBJECT

public:
  MyMainWindow();
  ~ MyMainWindow(){}
  Infotank * info;

public slots:
  void clickedSlot();
  void actualier_infotank();
};
#endif // MYMAINWINDOW_H
