#include "tank.h"
#include <QKeyEvent>
#include <QBrush>
#include <QColor>
#include "game.h"
#include <typeinfo>
#include "roche.h"
#include "impact.h"
#include <QPointF>
#include "qmath.h"

extern Game * game;

Tank::Tank(bool couleur)
{

    en_crevasse = false;
    peut_tirer = false;
    capacite = 80;

    /*setBrush(* new QBrush(QColor(77, 77, 77)));
    setRect(0,0,50,50);*/
    //setPos(20, 20);
    if(couleur)setPixmap(QPixmap(":/images/images/tankGreen.png"));
    else setPixmap(QPixmap(":/images/images/tankBlack.png"));


    canon = new Canon();

    canon->setRect(0,0,50,10);
    //canon->setPos(45,40);
    canon->setTransformOriginPoint(QPoint(0,5));
    canon->setAngle(0);
    canon->setL(50);
    canon->setDirection(0);


    //rayon degats quantite


    unitedep = new Unitedep(5,5);

    vie = 10;

    //Initialisation obus_type1
        obus[0][0]=1;
        obus[0][1]=2;
        obus[0][2]=-1;
        //Initialisation obus_type2
        obus[1][0]=5;
        obus[1][1]=5;
        obus[1][2]=10;
        //Initialisation obus_type3
        obus[2][0]=9;
        obus[2][1]=10;
        obus[2][2]=5;

}

void Tank::keyPressEvent(QKeyEvent *event){

    if (event->key() == Qt::Key_Left){
        if (pos().x() > 0)
        {
        traitement_mouvement(-unitedep->getLongueur(),0);
        }
    }
    else if (event->key() == Qt::Key_Right){
        if (pos().x() + 50 < 800)
        {
         traitement_mouvement(unitedep->getLongueur(),0);
        }
    }

    else if (event->key() == Qt::Key_Up){
        if (pos().y() > 0)
        {
         traitement_mouvement(0,-unitedep->getLageur());
        }

    }
    else if (event->key() == Qt::Key_Down){
        if (pos().y()+50 < 600)
        {
         traitement_mouvement(0,unitedep->getLageur());
        }
    }
    else if (event->key() == Qt::Key_S){

        canon->setRotation(((int)canon->rotation()+1) % 360);
        canon->setDirection((canon->getDirection() + 1) % 360);

    }
    else if (event->key() == Qt::Key_Z){

        canon->setRotation(((int)canon->rotation()-1)%360);
        canon->setDirection((canon->getDirection() - 1) % 360);

    }
    else if (event->key() == Qt::Key_Q){

        if(canon->getAngle() < 90){
            canon->setAngle(canon->getAngle()+1);
            canon->setRect(0,0,canon->getL()*(90-canon->getAngle())/90,10);
            }
    }
    else if (event->key() == Qt::Key_D){

        if(canon->getAngle() > 0){
            canon->setAngle(canon->getAngle()-1);
           canon->setRect(0,0,canon->getL()*(90-canon->getAngle())/90,10);
            }
    }
    else if (event->key() == Qt::Key_W){
        tirer(new Obus(obus[0][0],obus[0][1],obus[0][2]));
    }
    else if (event->key() == Qt::Key_X){
        if(obus[1][2] > 0 ){
            obus[1][2] = obus[1][2] - 1;
            tirer(new Obus(obus[1][0],obus[1][1],obus[1][2]));
        }

    }
    else if (event->key() == Qt::Key_C){
        if(obus[2][2] > 0 ){
            obus[2][2] = obus[2][2] - 1;
            tirer(new Obus(obus[2][0],obus[2][1],obus[2][2]));
        }

    }
    game->actualiser();
}


bool Tank::heurte_obstacle()
{
    QList<QGraphicsItem *> obstacles = collidingItems();
    for (int i = 0, n = obstacles.size(); i < n; ++i){
            if (dynamic_cast<Obstacle *>(obstacles[i])){
                return true;
            }
        }
    return false;


}

Crevasse * Tank::heurte_crevasse()
{
    Crevasse * res = NULL;
    QList<QGraphicsItem *> obstacles = collidingItems();
    for (int i = 0, n = obstacles.size(); i < n; ++i){
            if (res = dynamic_cast<Crevasse *>(obstacles[i])){
                // increase the score
                return res;
            }
        }
    return res;
}

void Tank::deplacer(int a, int b)
{
    setPos(x() + a ,y()+ b);
    canon->setPos(canon->x() + a,canon->y() + b);
}

void Tank::tirer(Obus *obus)
{
    if(peut_tirer)
    {
        double distance = 30 + 800*qSin(2* qDegreesToRadians(double(canon->getAngle())));
        double x1 = tete_canon().x();
        double y1 = tete_canon().y();
        QPointF * depart = new QPointF(x1,y1);
        double x2 = depart->x()+distance*qCos(qDegreesToRadians(double(canon->getDirection())));
        double y2 = depart->y()+distance*qSin(qDegreesToRadians(double(canon->getDirection())));

        //QPointF depart(canon->x()+5,canon->y+canon->l*(90-canon->angle)/90);
        //QPointF destination(depart.x()+distance*qCos(canon->direction),depart.y()+distance*qSin(canon->direction));

        QPointF * destination = new QPointF(x2,y2);
        Impact * impact = new Impact(destination);

        game->scene->addItem(impact);

        obus->setPos(depart->x(),depart->y());

        game->scene->addItem(obus);

        obus->setRotation((canon->getDirection()));

        obus->lancer();
        peut_tirer = false;
    }

}

Unitedep *Tank::getUnitedep()
{
    return unitedep;
}

Canon *Tank::getCanon()
{
    return canon;
}

bool Tank::getEncrevasse()
{
    return en_crevasse;
}

bool Tank::getPeuttirer()
{
    return peut_tirer;
}

int Tank::getCapacite()
{
    return capacite;
}

int Tank::getVie()
{
    return vie;
}

void Tank::setEncrevasse(bool val)
{
    en_crevasse = val;
}

void Tank::setPeuttirer(bool val)
{
    peut_tirer = val;
}

void Tank::setCapacite(int val)
{
    capacite = val;
}

void Tank::setVie(int val)
{
    vie = val;
}

QPointF Tank::tete_canon()
{
    double x = canon->x()+5+qCos(qDegreesToRadians(double(canon->getDirection())))*canon->getL()*(90- canon->getAngle())/90;
    double y = canon->y()+qSin(qDegreesToRadians(double(canon->getDirection())))*canon->getL()*(90- canon->getAngle())/90;
    return QPointF(x,y);
}

void Tank::positionner(int a, int b)
{
    setPos(a, b);
    canon->setPos(a + 25,b + 20);
}



void Tank::traitement_mouvement(int x, int y)
{
    if(capacite > 0){
        deplacer(x,y);
        if(heurte_obstacle())
        {
            deplacer(-x,-y);
        }
        if(Crevasse * crevasse_heurte =  heurte_crevasse())
        {
            if(! en_crevasse)
            {
                //lui soustraire la valeur du rayon de la crevasse dans laquelle il se trouve au lieu de 1
                capacite -= crevasse_heurte->getRayon();
                en_crevasse = true;
            }
        }
        else
        {
            en_crevasse = false;
        }
    }
}
