#ifndef IMPACT_H
#define IMPACT_H

#include <QGraphicsRectItem>


class Impact: public QObject, public QGraphicsRectItem

{
public:
    Impact(QPointF * position);
};

#endif // IMPACT_H
