QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = projetcpp

TEMPLATE = app

SOURCES += main.cpp \
    unitedep.cpp \
    tank.cpp \
    obus.cpp \
    canon.cpp \
    obstacle.cpp \
    arbre.cpp \
    roche.cpp \
    crevasse.cpp \
    eau.cpp \
    game.cpp \
    impact.cpp \
    infotank.cpp

RESOURCES += \
    ressources.qrc

HEADERS += \
    unitedep.h \
    tank.h \
    obus.h \
    canon.h \
    obstacle.h \
    arbre.h \
    roche.h \
    crevasse.h \
    eau.h \
    game.h \
    impact.h \
    infotank.h

FORMS +=
