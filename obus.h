#ifndef OBUS_H
#define OBUS_H
#include <QObject>
#include <QGraphicsRectItem>

class Obus: public QObject, public QGraphicsEllipseItem
{
    Q_OBJECT
public:
    QTimer * move_timer;
    Obus(int r, int d, int q);

    int getrayon();
    int getDegats();
    int getQuantite();

    void setRayaon(int val);
    void setDegats(int val);
    void setQuantite(int val);

    void lancer();

public slots:
    void se_deplacer();
private:
    int rayon,  degats,  quantite;
};

#endif // OBUS_H
