#include "crevasse.h"
#include <QBrush>
#include <QColor>

Crevasse::Crevasse(int rayon)
{
    this->rayon = rayon;
    setBrush(* new QBrush(QColor(76, 25, 0)));
    setRect(0,0,10*rayon,10*rayon);
    //setPos( qrand() % (751), qrand() % (551));

}

void Crevasse::setRayon(int val)
{
    rayon = val;
}

int Crevasse::getRayon()
{
    return rayon;
}

void Crevasse::centraliser()
{
    setPos(x()- boundingRect().width()/2,y()-boundingRect().height()/2);

}
